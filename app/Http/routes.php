<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware'=>'web'], function(){


    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('hello', function () {
        return "Hello World";
    });

    Route::get('users', 'userController@index');

    Route::get('user/create', function () {
        return "Create user form";
    });

    Route::post('user/create', 'userController@createUser');

    Route::get('user/edit/{id}', 'userController@editUserForm');

    Route::post('user/edit/{id}', 'userController@editUser');

    Route::post('user/delete/{id}', 'userController@deleteUser');



});

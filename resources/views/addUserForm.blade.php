@extends('layouts.master')

@section('content')
    <div class="container">

        <div class="well col-md-6 col-md-offset-3">
            <h1>Add User Form</h1>
            {{Form::open(array('url'=>'user/create'))}}
            <label for="name">Name</label>
            <input type="text" name="name" placeholder="Name" class="form-control">
            <label for="username">Username</label>
            <input type="text" name="username" placeholder="Username" class="form-control">
            <label for="email">Email</label>
            <input type="text" name="email" placeholder="Email" class="form-control">
            <label for="Password">Password</label>
            <input type="password" name="password" placeholder="Password" class="form-control">
            <input type="submit" value="Add user" class="btn btn-primary">

            {{Form::close()}}
        </div>
    </div>
@endsection
